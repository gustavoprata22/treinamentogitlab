-- QUERYS para treinamento Java Spring NTT 12-16 set 2022

CREATE TABLE tb_pages (
    id_page int(5) NOT NULL,
    nm_pages varchar(60) NOT NULL,
    fg_ativo char(1)  NOT NULL,
    data_atualizacao datetime NOT NULL,
    data_criacao varchar(60)  NULL,

    PRIMARY KEY (id_page)
);



CREATE TABLE tb_tool (
    id_tool int(5) NOT NULL,
    nm_tool varchar(60) NOT NULL,
    id_dcs int(5) NOT NULL,
    id_nichos int(5) NOT NULL,
    nm_link varchar(60) NOT NULL,
    nm_image varchar(60) NOT NULL,
    fg_ativo char(1)  NOT NULL,
    data_atualizacao datetime NOT NULL,
    data_criacao varchar(60)  NULL,

    PRIMARY KEY (id_tool)
);

ALTER TABLE tb_tool ADD CONSTRAINT id_dcs_tools
FOREIGN KEY (id_dcs) REFERENCES tb_datacenters(id_dc);

ALTER TABLE tb_tool ADD CONSTRAINT id_nichos
FOREIGN KEY (id_nichos) REFERENCES tb_nicho(id_nicho);


SELECT DISTINCT(TT.nm_tool), TT.nm_link, TT.nm_image, TN.nm_nicho, TD.nm_dc, TP.nm_pages FROM tb_pages TP, tb_tool TT, tb_grupo TG, tb_nicho TN, tb_datacenters TD
WHERE nm_grupo in ('dummy', 'dummy')
AND TT.fg_ativo = 'S'
and TG.fg_ativo = 'S'
and TP.fg_ativo = 'S'
and TN.id_nicho = TT.id_nichos
and TD.id_dc = TT.id_dcs
and TP.id_page = TG.id_pages
and TT.id_tool = TG.id_tools;